# Terraform Google PubSub
Pub/Sub is an asynchronous messaging service that decouples services that produce events from services that process events.

You can use Pub/Sub as messaging-oriented middleware or event ingestion and delivery for streaming analytics pipelines.

Pub/Sub offers durable message storage and real-time message delivery with high availability and consistent performance at scale. Pub/Sub servers run in all Google Cloud regions around the world.

### Provisioned Resources
* Pub/Sub Subscription

## Requirements
* Pub/Sub Topic ID

## Providers
| Name  | Version |
|-------|---------|
|  gcp  |   n/a   |