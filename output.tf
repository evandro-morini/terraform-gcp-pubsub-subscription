output "google_pubsub_subscription_id" {
    value = "${google_pubsub_subscription.this.id}"
}

output "google_pubsub_subscription_name" {
    value = "${google_pubsub_subscription.this.name}"
}