resource "google_pubsub_subscription" "this" {
  name = var.subscription_name
  topic = var.topic_id
  labels = var.labels
  project =  var.project_id
  message_retention_duration = var.message_retention_duration
  retain_acked_messages = var.retain_acked_messages
  ack_deadline_seconds = var.ack_deadline_seconds
  enable_message_ordering = var.enable_message_ordering
  filter = var.filter

  dynamic "push_config" {
    for_each = var.push_config ? [1] : []
    content {
      push_endpoint = var.push_endpoint
      attributes = var.attributes
    }
  }

  dynamic "expiration_policy" {
    for_each = var.expiration_policy ? [1] : []
    content {
      ttl = var.ttl
    }
  }

  dynamic "dead_letter_policy" {
    for_each = var.dead_letter_policy ? [1] : []
    content {
      dead_letter_topic = var.dead_letter_topic
      max_delivery_attempts = var.max_delivery_attempts
    }
  }

  dynamic "retry_policy" {
    for_each = var.retry_policy ? [1] : []
    content {
      minimum_backoff = var.minimum_backoff
      maximum_backoff = var.maximum_backoff
    }
  }
}