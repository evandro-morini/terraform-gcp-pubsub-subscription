variable "subscription_name" {
  type = string
  description = "Subscription Name"
}

variable "topic_id" {
  type = string
  description = "PubSub Topic ID"
}

variable "labels" {
  type = map
  description = "Key:Value labels"
  default = {}
}

variable "project_id" {
  type = string
  description = "Project ID"
}

variable "push_config" {
  type = bool
  description = "push_config block conditional"
  default = false
}

variable "push_endpoint" {
  type = string
  description = "A URL locating the endpoint to which messages should be pushed"
  default = ""
}

variable "attributes" {
    type = map
    description = "Endpoint configuration attributes"
    default = {
      x-goog-version = "v1"
    }
}

variable "ack_deadline_seconds" {
  type = number
  description = "The maximum time after a subscriber receives a message before the subscriber should acknowledge the message"
  default = 10
}

variable "retain_acked_messages" {
  type = bool
  description = "Indicates whether to retain acknowledged messages"
  default = false
}

variable "message_retention_duration" {
  type = string
  description = "How long to retain unacknowledged messages in the subscription's backlog"
  default = "604800s"
}

variable "expiration_policy" {
  type = bool
  description = "expiration_policy block conditional"
  default = true
}

variable "ttl" {
  type = string
  description = "A time for subscription's expiration (empty equals unlimited)"
  default = ""
}

variable "filter" {
  type = string
  description = "The subscription only delivers the messages that match the filter"
  default = ""
}

variable "dead_letter_policy" {
  type = bool
  description = "dead_letter_policy block conditional"
  default = false
}

variable "dead_letter_topic" {
  type = string
  description = "The name of the topic to which dead letter messages should be published"
  default = ""
}

variable "max_delivery_attempts" {
  type = number
  description = "The maximum number of delivery attempts for any message"
  default = 0
}

variable "retry_policy" {
  type = bool
  description = "retry_policy block conditional"
  default = false
}

variable "minimum_backoff" {
  type = string
  description = "The minimum delay between consecutive deliveries of a given message. Default: 10s"
  default = "10.0s"
}

variable "maximum_backoff" {
  type = string
  description = "The maximum delay between consecutive deliveries of a given message"
  default = "600.0s"
}

variable "enable_message_ordering" {
  type = bool
  description = "messages published with the same orderingKey in PubsubMessage will be delivered to the subscribers in the order in which they are received"
  default = false
}

